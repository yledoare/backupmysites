# Yann Le Doaré
#!/bin/bash

if [ -e $HOME/.config/backupmysites.conf ] 
then
	source $HOME/.config/backupmysites.conf
else
	# Make changes here
	mailfrom="myname@mydomain"
	morebackup="http://mydomain/uploader.php"
fi

install -d $HOME/sites/logs
install -d $HOME/sites/archives
install -d $HOME/sites/sql
install -d $HOME/sites/html
cd $HOME/sites
DAY=`date "+%d"`
MONTH=`date "+%m"`
DATE=$DAY
[ "$DAY" = "01" ] && DATE=`date "+%d-%m-%Y"`
CONFIG_FILE="$HOME/.filezilla/sitemanager.xml"
[ -e $HOME/test-sitemanager.xml ] && CONFIG_FILE=$HOME/test-sitemanager.xml
cat $CONFIG_FILE |while read line 
do
	echo $line | grep Host 1>/dev/null && HOST="`echo $line |cut -d '>' -f2 |cut -d '<' -f1`"
	echo $line | grep User 1>/dev/null && USER="`echo $line |cut -d '>' -f2 |cut -d '<' -f1`"
	echo $line | grep Pass 1>/dev/null && PASS="`echo $line |cut -d '>' -f2 |cut -d '<' -f1`"
	echo $line | grep RemoteDir 1>/dev/null && REMOTEDIR="`echo $line |cut -d '>' -f2 |cut -d '<' -f1|cut -d' ' -f4`" 
	echo $line | grep '</Server>' 1>/dev/null && DOIT="OK"
	if [ "$DOIT" = "OK" ]
	then
		if [ "$REMOTEDIR" = "" ]
		then
			REMOTEDIR="www"
		fi
		LOCALDIR=$REMOTEDIR.$USER/$REMOTEDIR 

		[ "$HOST" = "ftpperso.free.fr" ] && DOIT="" && continue
		URL=`echo $HOST | sed "+s/ftp/$REMOTEDIR/"`
		[ -e $HOME/sites/html/$URL.html ] && mv $HOME/sites/html/$URL.html $HOME/sites/html/$URL.html.back
		wget -q $URL --output-document=$HOME/sites/html/$URL.html
		if [ -e $HOME/sites/html/$URL.html.back ]
		then
			diff $HOME/sites/html/$URL.html.back $HOME/sites/html/$URL.html > $HOME/sites/logs/$URL.diff.$DATE.log
			if [ $? != 0 ] 
			then
				COMPTEUR=0
				diff $HOME/sites/html/$URL.html.back $HOME/sites/html/$URL.html | while read
			do
				COMPTEUR=`expr $COMPTEUR + 1`
				# [ $COMPTEUR -gt 15 ] && uuencode $URL.html $URL.html | mail -s "Changement page accueil $URL" $mailfrom && break
				[ $COMPTEUR -gt 25 ] && diff $HOME/sites/html/$URL.html.back $HOME/sites/html/$URL.html | mail -s "Changement page accueil $URL" $mailfrom && break
			done
		fi
	fi

	#	install -d $LOCALDIR
	host $HOST 1>/dev/null # sometimes dns are slow for response 
	echo "Host : $HOST, user : $USER, pass : XXX, remote dir /$REMOTEDIR"
	ncftpget -R -u $USER -p $PASS $HOST $REMOTEDIR.$USER /$REMOTEDIR 1>$HOME/sites/logs/$REMOTEDIR.$USER.$DATE.log 2>$HOME/sites/logs/$REMOTEDIR.$USER.$DATE.log
if [ ! $? = 0 ]
then
	uuencode $HOME/sites/logs/$REMOTEDIR.$USER.$DATE.log  $HOME/sites/logs/$REMOTEDIR.$USER.$DATE.log | mail -s "Erreur $REMOTEDIR.$USER.$DATE" $mailfrom
	DOIT="" && continue
fi
tar czf $HOME/sites/archives/$REMOTEDIR.$USER.$DATE.tar.gz $LOCALDIR
if [ -e "$LOCALDIR/client/config_thelia.php" ]
then
	echo "Backup Thelia database"
	DATABASE=`grep THELIA_BD_NOM $LOCALDIR/client/config_thelia.php | cut -d"'" -f4`
	SQLPASS=`grep THELIA_BD_PASSWORD $LOCALDIR/client/config_thelia.php | cut -d"'" -f4`
	SQLUSER=`grep THELIA_BD_LOGIN $LOCALDIR/client/config_thelia.php | cut -d"'" -f4`
	SQLHOST=`grep THELIA_BD_HOST $LOCALDIR/client/config_thelia.php | cut -d"'" -f4`
fi
if [ -e "$LOCALDIR/wp-config.php" ]
then
	echo "Backup Wordpress database"
	DATABASE=`grep DB_NAME $LOCALDIR/wp-config.php | cut -d"'" -f4`
	SQLPASS=`grep DB_PASSWORD $LOCALDIR/wp-config.php | cut -d"'" -f4`
	SQLUSER=`grep DB_USER $LOCALDIR/wp-config.php | cut -d"'" -f4`
	SQLHOST=`grep DB_HOST $LOCALDIR/wp-config.php | cut -d"'" -f4`
fi
# backup Prestashop 1.6
if [ -e "$LOCALDIR/config/settings.inc.php" ]
then
	echo "Prestashop 1.6"
	SQLHOST=$(grep _DB_SERVER_ $LOCALDIR/config/settings.inc.php|cut -d"'" -f4)
	DATABASE=$(grep _DB_NAME_ $LOCALDIR/config/settings.inc.php|cut -d"'" -f4)
	SQLUSER=$(grep _DB_USER_ $LOCALDIR/config/settings.inc.php|cut -d"'" -f4)
	SQLPASS=$(grep _DB_PASSWD_ $LOCALDIR/config/settings.inc.php|cut -d"'" -f4)
fi
# backup SQL Joomal 1.5
if [ -e "$LOCALDIR/configuration.php" ]
then
	SQLPASS=`grep "\\$password =" $LOCALDIR/configuration.php|cut -d"'" -f2`
	SQLUSER=`grep "\\$user =" $LOCALDIR/configuration.php|cut -d"'" -f2`
	SQLHOST=`grep "\\$host =" $LOCALDIR/configuration.php|cut -d"'" -f2`
	DATABASE=`grep "\\$db =" $LOCALDIR/configuration.php|cut -d"'" -f2`
	MAILOWNER=`grep mailfrom lileauxm/configuration.php|cut -d"'" -f2`
fi
# backup SQL DRUPAL
if [ -e "$LOCALDIR/sites/default/settings.php" ]
then
	CFG=`grep db_url $LOCALDIR/sites/default/settings.php | grep -v '*' | head -1`
	if [ "$CFG" != "" ]
	then
		# Drupal 6
		echo "SQL cfg is $CFG"
		SQLUSER=`echo $CFG | cut -d'/' -f3 | cut -d':' -f1`
		SQLPASS=`echo $CFG | cut -d'/' -f3 | cut -d':' -f2 | cut -d'@' -f1`
		SQLHOST=`echo $CFG | cut -d'/' -f3 | cut -d':' -f2 | cut -d'@' -f2`
		DATABASE=`echo $CFG | cut -d'/' -f4 | cut -d"'" -f1`
	else
		DATABASE=`grep "      'database" $LOCALDIR/sites/default/settings.php | cut -d "'" -f4`
		SQLUSER=`grep "      'user" $LOCALDIR/sites/default/settings.php | cut -d "'" -f4`
		SQLPASS=`grep "      'password" $LOCALDIR/sites/default/settings.php | cut -d "'" -f4`
		SQLHOST=`grep "      'host" $LOCALDIR/sites/default/settings.php | cut -d "'" -f4`
	fi
fi
if [ "$SQLUSER" != "" ] && [ "$SQLPASS" != "" ] && [ "$SQLHOST" != "" ] && [ "$DATABASE" != "" ]
then
	echo "SQL Backup"
	echo "
	<?  \$last_line = system(\"mysqldump --host=$SQLHOST --user=$SQLUSER --password=$SQLPASS $DATABASE > ../$DATABASE.$DATE.sql\",\$retval); ?>
	<?  echo \" \$retval \$last_line; \" ?>
	<?  \$last_line = system(\"gzip ../$DATABASE.$DATE.sql\",\$retval); ?>
	<?  echo \$last_line; ?>
	<?  echo \" \$retval \$last_line; \" ?>
	" > yann-backup.php
	/bin/echo -e "cd $REMOTEDIR \n put yann-backup.php" | ncftp -u $USER -p $PASS $HOST
	rm yann-backup.php
	wget --http-user=david --http-passwd=yann $URL/yann-backup.php
	mv yann-backup.php $HOME/sites/logs/$REMOTEDIR.$USER.$DATE.sql.log
	/bin/echo -e "get $DATABASE.$DATE.sql.gz \n rm $DATABASE.$DATE.sql.gz" | ncftp -u $USER -p $PASS $HOST
	curl -F "uploadedfile=@""$DATABASE.$DATE.sql.gz" "$morebackup?client=$REMOTEDIR.$USER"
	mv "$DATABASE.$DATE.sql.gz" $HOME/sites/sql
fi
rm -fR $LOCALDIR
curl -F "uploadedfile=@""$HOME/sites/archives/$REMOTEDIR.$USER.$DATE.tar.gz" "$morebackup?client=$REMOTEDIR.$USER"
if [ "$MAILOWNER" != "" ] && [ "$DAY" = "01" ]
then
	echo "envoi a $MAILOWNER"
	echo "Merci de conserver ce mail ou de telecharger le fichier joint" | mail  -a $HOME/sites/archives/$LOCALDIR.$DATE.tar.gz -s "Sauvegarde fichiers site internet (robot)" -c $mailfrom $MAILOWNER
	if [ -e $HOME/sites/sql/$DATABASE.$DATE.sql.gz ]
	then	
		echo "Merci de conserver ce mail ou de telecharger le fichier joint" | mail -a $HOME/sites/sql/$DATABASE.$DATE.sql.gz -s "Sauvegarde base donee site internet (robot)" -c $mailfrom $MAILOWNER
	fi

fi
DOIT=""
SQLUSER=""
MAILOWNER=""
fi
done
# SQL Free
# curl -s -S -O -D curl.headers -d "login=$user&password=$password&check=1&all=1" http://sql.free.fr/backup.php
